package com.dreamerstrendyol.marketplace.Configurations;

import com.couchbase.client.java.Cluster;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class CouchbaseIndexConfiguration {

    private final Cluster couchbaseCluster;

    public CouchbaseIndexConfiguration(Cluster couchbaseCluster) {
        this.couchbaseCluster = couchbaseCluster;
    }

    @Bean
    public void createIndexes() {
       try{
           couchbaseCluster.query("CREATE INDEX gameIcon ON market(gameIcon);");
           couchbaseCluster.query("CREATE INDEX gameName ON market(gameName);");
           couchbaseCluster.query("CREATE INDEX genre ON market(genre);");
           couchbaseCluster.query("CREATE INDEX hasAdultContent ON market(hasAdultContent);");
           couchbaseCluster.query("CREATE INDEX internetSite ON market(internetSite);");
           couchbaseCluster.query("CREATE INDEX languages ON market(languages);");
           couchbaseCluster.query("CREATE INDEX platform ON market(platform);");
           couchbaseCluster.query("CREATE INDEX price ON market(price);");
           couchbaseCluster.query("CREATE INDEX publisherCompany ON market(publisherCompany);");
           couchbaseCluster.query("CREATE INDEX publisherSite ON market(publisherSite);");
           couchbaseCluster.query("CREATE INDEX releaseDate ON market(releaseDate);");
           couchbaseCluster.query("CREATE INDEX gameId ON market(salesCount);");
           couchbaseCluster.query("CREATE INDEX systemRequirements ON market(systemRequirements);");
           couchbaseCluster.query("CREATE INDEX tags ON market(tags);");
           couchbaseCluster.query("CREATE INDEX userRating ON market(userRating);");
       }catch (Exception e){
           System.out.println(e.getMessage());
       }
    }
}


