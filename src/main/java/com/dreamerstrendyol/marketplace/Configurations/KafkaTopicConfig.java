package com.dreamerstrendyol.marketplace.Configurations;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic userTryBuyGame() {
        return TopicBuilder.name("userBuyGame").build();
    }
    @Bean
    public NewTopic announcement() {
        return TopicBuilder.name("announcement").build();
    }
    @Bean
    public NewTopic discountGame() {
        return TopicBuilder.name("discountGame").build();
    }
}
