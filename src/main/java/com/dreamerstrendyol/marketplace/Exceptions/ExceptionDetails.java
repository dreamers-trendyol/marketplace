package com.dreamerstrendyol.marketplace.Exceptions;

import lombok.Getter;

import java.util.Date;

@Getter
public class ExceptionDetails {
    private final String errorType;
    private final String errorMessage;
    private final String requestInfo;
    private final Date time;

    public ExceptionDetails(String errorType, String errorMessage, String requestInfo, Date time) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.requestInfo = requestInfo;
        this.time = time;
    }
}
