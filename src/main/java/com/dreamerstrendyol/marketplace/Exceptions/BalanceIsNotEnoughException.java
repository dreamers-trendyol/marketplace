package com.dreamerstrendyol.marketplace.Exceptions;

public class BalanceIsNotEnoughException extends RuntimeException {
    public BalanceIsNotEnoughException(String message) {
        super(message);
    }
}
