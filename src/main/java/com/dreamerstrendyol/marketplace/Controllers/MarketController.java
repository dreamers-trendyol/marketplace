package com.dreamerstrendyol.marketplace.Controllers;

import com.dreamerstrendyol.marketplace.Contracts.Request.CreateGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Request.PatchGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Request.ReviewValidationDTO;
import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Helper.PatchHelper;
import com.dreamerstrendyol.marketplace.Models.Announcement;
import com.dreamerstrendyol.marketplace.Models.Game;
import com.dreamerstrendyol.marketplace.Models.Review;
import com.dreamerstrendyol.marketplace.Services.MarketService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/market")
public class MarketController {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private MarketService marketService;

    @GetMapping("/games")
    public ResponseEntity<List<Game>> getAllGames() {
        return ResponseEntity.ok(marketService.getAllGames());
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<Game> getGameById(@PathVariable String gameId) {
        Game game = marketService.getGameById(gameId);
        return ResponseEntity.ok(game);
    }

    @PostMapping("/games")
    public ResponseEntity<Void> createGame(@Valid @RequestBody CreateGameRequest createGameRequest) {
        String gameId = marketService.createGame(createGameRequest);
        URI location = URI.create(String.format("/v1/market/games/%s/", gameId));
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/games/{gameId}")
    public ResponseEntity<Void> deleteGame(@PathVariable String gameId) {
        marketService.deleteGame(gameId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/games/{gameId}/price")
    public ResponseEntity<Double> getPrice(@PathVariable String gameId) {
        return ResponseEntity.ok(marketService.getPrice(gameId));
    }

    @PatchMapping("/games/{gameId}/price")
    public ResponseEntity<Void> setPrice(@RequestBody PatchGameRequest request, @PathVariable String gameId) {
        marketService.setPrice(gameId,request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/games/{gameId}/salesCount")
    public ResponseEntity<Integer> getSalesCount(@PathVariable String gameId) {
        return ResponseEntity.ok(marketService.getSalesCount(gameId));
    }

    @PostMapping("/games/{gameId}/reviews")
    public ResponseEntity<Void> addReview(@RequestBody Review review, @PathVariable String gameId) {
        marketService.addReview(gameId, review);
        URI location = URI.create(String.format("/v1/market/games/%s/reviews/%s/", gameId, review.getReviewId()));
        return ResponseEntity.created(location).build();
    }

    @PatchMapping("/games/{gameId}/reviews/{reviewId}")
    public ResponseEntity<Void> updateReview(@RequestBody ReviewValidationDTO reviewDTO, @PathVariable String gameId, @PathVariable String reviewId) {
        try {
            JsonPatch patch = PatchHelper.createJsonPatch(reviewDTO);
            marketService.updateReview(patch, gameId, reviewId);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/games/{gameId}/reviews/{reviewId}")
    public ResponseEntity<Void> deleteReview(@PathVariable String reviewId, @PathVariable String gameId) {
        marketService.deleteReview(gameId, reviewId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/games/{gameId}/reviews/{reviewId}")
    public ResponseEntity<Review> getReviewById(@PathVariable String reviewId, @PathVariable String gameId) {
        return ResponseEntity.ok(marketService.getReviewById(gameId, reviewId));
    }

    @GetMapping("/games/{gameId}/rating")
    public ResponseEntity<Integer> getUserRating(@PathVariable String gameId) {
        return ResponseEntity.ok(marketService.getUserRating(gameId));
    }

    @GetMapping("/games/{gameId}/reviews")
    public ResponseEntity<List<Review>> getReviewsByUserName(@RequestParam String userName, @PathVariable String gameId) {
        if (userName.equals("-"))//Username - ise tüm reviewları değil ise ilgili username e ait reviewlaarı getirir.
            return ResponseEntity.ok(marketService.getReviews(gameId));
        return ResponseEntity.ok(marketService.getReviewsByUserName(gameId, userName));
    }

    @PostMapping("/games/{gameId}/buy")
    public ResponseEntity<Void> buyGame(@PathVariable String gameId, @RequestBody String userId) {
        marketService.buyGame(gameId, userId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/games/{gameId}/announcement")
    public ResponseEntity<Void> createAnnouncement(@PathVariable String gameId, @RequestBody Announcement announcement) {
        marketService.createAnnouncement(gameId, announcement);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity<Void> updateGame(@RequestBody PatchGameRequest request, @PathVariable String gameId) throws JsonPatchException, JsonProcessingException {
        JsonPatch patch = null;
        try {
            patch = PatchHelper.createJsonPatch(request);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        marketService.updateGame(gameId, patch);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/trending-games")
    public ResponseEntity<List<TopGameResponse>> getTrendingGames(@RequestParam(required = false, defaultValue = "1") int page, @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(marketService.getTrendingGames((page - 1) * limit, limit));
    }

    @GetMapping("/top-seller-games")
    public ResponseEntity<List<TopGameResponse>> getTopSellerGames(@RequestParam(required = false, defaultValue = "1") int page, @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(marketService.getTopSellerGames((page - 1) * limit, limit));
    }

    @GetMapping("/new-releases-games")
    public ResponseEntity<List<TopGameResponse>> getNewReleasesGames(@RequestParam(required = false, defaultValue = "1") int page, @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(marketService.getNewReleasesGames((page - 1) * limit, limit));
    }

    @GetMapping("/upcoming-games")
    public ResponseEntity<List<UpcomingGameResponse>> getUpcomingGames(@RequestParam(required = false, defaultValue = "1") int page, @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(marketService.getUpcomingGames((page - 1) * limit, limit));
    }

}
