package com.dreamerstrendyol.marketplace.Repositories;

import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Models.Game;
import com.dreamerstrendyol.marketplace.Models.Review;

import java.util.List;

public interface MarketRepository {
    List<Game> getAllGames();
    Game getGameById(String gameId);
    String createGame(Game game);
    void deleteGame(String gameId);
    void updateGame(Game game);
    Double getPrice(String gameId);
    int getSalesCount(String gameId);
    List<Review> getReviews(String gameId);
    void addReview(String gameId, Game game);
    void deleteReview(String gameId, Game game);
    Review getReviewById(String gameId, String reviewId);
    int getUserRating(String gameId);
    List<Review> getReviewsByUserName(String gameId, String userName);
    List<TopGameResponse> getTrendingGames(int offset, int limit);
    List<TopGameResponse> getTopSellerGames(int offset, int limit);
    List<TopGameResponse> getNewReleasesGames(int offset, int limit, String now, String beforeAWeek);
    List<UpcomingGameResponse> getUpcomingGames(int offset, int limit, String now, String afterTwoMonths);
}
