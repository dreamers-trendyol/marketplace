package com.dreamerstrendyol.marketplace.Models;

import lombok.Getter;
import org.json.JSONObject;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
@Getter
public class KafkaConsumer {

    @KafkaListener(topics = "deneme2")
    void userTryBuyGameListener(String data) {
        System.out.println(data);
    }
}


