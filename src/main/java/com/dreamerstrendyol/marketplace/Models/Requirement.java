package com.dreamerstrendyol.marketplace.Models;

import java.util.UUID;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Requirement {
    private String requirementId = UUID.randomUUID().toString();
    private String requirementType;
    private String os;
    private String cpu;
    private String ram;
    private String graphicsCard;
    private String storage;
}
