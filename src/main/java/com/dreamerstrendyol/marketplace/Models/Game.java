package com.dreamerstrendyol.marketplace.Models;

import lombok.*;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Game implements Serializable {
    private String gameId ;
    private String gameName;
    private String gameDescription;
    private String publisherCompany;
    private String genre;
    private String internetSite;
    private String publisherSite;
    private double price;
    private int userRating;
    private String gameIcon;
    private int salesCount;
    private String hasAdultContent;

    private String releaseDate;
    private String updatedAt;

    private String[] tags;
    private String[] platform;
    private String[] gameMediaContainer;

    private List<Requirement> systemRequirements;

    private List<Language> languages;
    private List<Review> reviews;

    public Game(String gameName, String gameDescription, String publisherCompany, String genre, String internetSite, String publisherSite, double price, String gameIcon, String hasAdultContent, String releaseDate, String[] tags, String[] platform, String[] gameMediaContainer, List<Requirement> systemRequirements, List<Language> languages) {
        this.gameId = UUID.randomUUID().toString();
        this.gameName = gameName;
        this.gameDescription = gameDescription;
        this.publisherCompany = publisherCompany;
        this.genre = genre;
        this.internetSite = internetSite;
        this.publisherSite = publisherSite;
        this.price = price;
        this.gameIcon = gameIcon;
        this.hasAdultContent = hasAdultContent;
        this.releaseDate = releaseDate;
        this.tags = tags;
        this.platform = platform;
        this.gameMediaContainer = gameMediaContainer;
        this.systemRequirements = systemRequirements;
        this.languages = languages;
        this.salesCount = 0;
        this.reviews = new ArrayList<Review>();
        this.updatedAt = LocalDate.now().toString();
        this.userRating = 0;
    }

    public Review getReviewById(String reviewId){
        return this.getReviews().stream()
                .filter(r-> r.getReviewId().equals(reviewId)).findFirst().get();
    }

    public void replaceReviewById(Review review, Review reviewById){
        this.getReviews().remove(reviewById);
        this.getReviews().add(review);
    }
}
