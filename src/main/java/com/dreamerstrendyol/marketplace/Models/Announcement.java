package com.dreamerstrendyol.marketplace.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Announcement {

    private String title;
    private String message;
}
