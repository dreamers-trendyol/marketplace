package com.dreamerstrendyol.marketplace.Models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Language {
    String languageName;
    Boolean userInterface;
    Boolean fullAudio;
    Boolean subtitle;
}
