package com.dreamerstrendyol.marketplace.Models;

import lombok.*;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Review {
    private String reviewId = UUID.randomUUID().toString();
    private String userId;
    private String userName;
    private Boolean adviceIt;
    private String reviewContent;
    private String createdAt;
    private String updatedAt;
}
