package com.dreamerstrendyol.marketplace.Services;

import com.dreamerstrendyol.marketplace.Contracts.Request.CreateGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Request.PatchGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Models.Announcement;
import com.dreamerstrendyol.marketplace.Models.Game;
import com.dreamerstrendyol.marketplace.Models.Review;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import java.util.List;

public interface MarketService {
    List<Game> getAllGames();
    String createGame(CreateGameRequest createGameRequest);
    Game getGameById(String gameId);
    void deleteGame(String gameId);
    Double getPrice(String gameId);
    void setPrice(String gameId, PatchGameRequest patchGameRequest);
    int getSalesCount(String gameId);
    List<Review> getReviews(String gameId);
    String addReview(String gameId, Review review);
    void deleteReview(String gameId, String reviewId);
    Review getReviewById(String gameId, String reviewId);
    void updateReview(JsonPatch patch, String gameId, String reviewId);
    int getUserRating(String gameId);
    List<Review> getReviewsByUserName(String gameId, String userName);
    void buyGame(String gameId, String userId);
    void createAnnouncement(String gameId, Announcement announcement);
    void setPriceByGameId(String gameId, JsonPatch patch) throws JsonPatchException, JsonProcessingException;
    void updateGame(String gameId, JsonPatch patch) throws JsonPatchException, JsonProcessingException;
    List<TopGameResponse> getTrendingGames(int offset, int limit);
    List<TopGameResponse> getTopSellerGames(int offset, int limit);
    List<TopGameResponse> getNewReleasesGames(int offset, int limit);
    List<UpcomingGameResponse> getUpcomingGames(int offset, int limit);
}
