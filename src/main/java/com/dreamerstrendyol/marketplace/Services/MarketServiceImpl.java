package com.dreamerstrendyol.marketplace.Services;

import com.dreamerstrendyol.marketplace.Contracts.Request.CreateGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Request.PatchGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Exceptions.BalanceIsNotEnoughException;
import com.dreamerstrendyol.marketplace.Helper.PatchHelper;
import com.dreamerstrendyol.marketplace.Models.Announcement;
import com.dreamerstrendyol.marketplace.Models.Game;
import com.dreamerstrendyol.marketplace.Models.KafkaSender;
import com.dreamerstrendyol.marketplace.Models.Review;
import com.dreamerstrendyol.marketplace.Repositories.MarketRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import lombok.AccessLevel;
import lombok.Setter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class MarketServiceImpl implements MarketService {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    RestTemplate getRestTemplate;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private MarketRepository marketRepository;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private KafkaSender sender;

    @Override
    public List<Game> getAllGames() {
        return marketRepository.getAllGames();
    }

    @Override
    public String createGame(CreateGameRequest createGameRequest) {
        Game game = new Game(createGameRequest.getGameName(), createGameRequest.getGameDescription(),
                createGameRequest.getPublisherCompany(), createGameRequest.getGenre(), createGameRequest.getInternetSite(),
                createGameRequest.getPublisherSite(), createGameRequest.getPrice(), createGameRequest.getGameIcon(),
                createGameRequest.getHasAdultContent(), createGameRequest.getReleaseDate(), createGameRequest.getTags(),
                createGameRequest.getPlatform(), createGameRequest.getGameMediaContainer(), createGameRequest.getSystemRequirements(),
                createGameRequest.getLanguages());
        return marketRepository.createGame(game);
    }

    @Override
    public Game getGameById(String gameId) {
        return marketRepository.getGameById(gameId);
    }

    @Override
    public void deleteGame(String gameId) {
        marketRepository.deleteGame(gameId);
    }

    @Override
    public Double getPrice(String gameId) {
        return marketRepository.getPrice(gameId);
    }

    @Override
    public int getSalesCount(String gameId) {
        return marketRepository.getSalesCount(gameId);
    }

    @Override
    public List<Review> getReviews(String gameId) {
        return marketRepository.getReviews(gameId);
    }

    @Override
    public String addReview(String gameId, Review review) {
        Game game = marketRepository.getGameById(gameId);
        review.setCreatedAt(LocalDate.now().toString());
        review.setUpdatedAt(LocalDate.now().toString());
        game.getReviews().add(review);
        marketRepository.addReview(gameId, game);
        return review.getReviewId();
    }

    @Override
    public void deleteReview(String gameId, String reviewId) {
        Game game = marketRepository.getGameById(gameId);
        Review review = game.getReviews().stream().filter(r -> r.getReviewId().equals(reviewId)).findFirst().get();
        game.getReviews().remove(review);
        marketRepository.deleteReview(gameId, game);
    }

    @Override
    public Review getReviewById(String gameId, String reviewId) {
        return marketRepository.getReviewById(gameId, reviewId);
    }

    @Override
    public void setPrice(String gameId, PatchGameRequest patchGameRequest) {
        try {
            Game game = marketRepository.getGameById(gameId);
            JsonPatch patch = PatchHelper.createJsonPatch(patchGameRequest);
            Game gamePatched = PatchHelper.applyPatch(patch, game);
            marketRepository.updateGame(gamePatched);
            checkPriceDecreasedAndSendMessage(game, gamePatched);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JsonPatchException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void checkPriceDecreasedAndSendMessage(Game game, Game gamePatched) {
        if (game.getPrice() > gamePatched.getPrice()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("senderServiceName", "marketplace::" + new Date());
            jsonObject.put("gameId", game.getGameId());
            jsonObject.put("price", gamePatched.getPrice());
            sender.sendMessage(jsonObject.toString(), "discountGame");
        }
    }

    @Override
    public void updateReview(JsonPatch patch, String gameId, String reviewId) {
        Game game = marketRepository.getGameById(gameId);
        Review reviewById = game.getReviewById(reviewId);
        try {
            Review review = PatchHelper.applyPatch(patch, reviewById);
            game.replaceReviewById(review, reviewById);
            marketRepository.updateGame(game);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getUserRating(String gameId) {
        return marketRepository.getUserRating(gameId);
    }

    @Override
    public List<Review> getReviewsByUserName(String gameId, String userName) {
        return marketRepository.getReviewsByUserName(gameId, userName);
    }

    @Override
    public void buyGame(String gameId, String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        Double balanceOfUser = getRestTemplate.exchange("http://users.cloudns.cl:8080/v1/user/" + userId + "/balance",
                HttpMethod.GET, entity, Double.class).getBody();
        Game game = marketRepository.getGameById(gameId);
        if (balanceOfUser >= game.getPrice()) {
            game.setSalesCount(game.getSalesCount() + 1);
            marketRepository.updateGame(game);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("senderServiceName", "marketplace::" + new Date());
            jsonObject.put("userId", userId);
            jsonObject.put("gameId", gameId);
            jsonObject.put("gameName", game.getGameName());
            jsonObject.put("price", game.getPrice());
            sender.sendMessage(jsonObject.toString(), "userBuyGame");
        } else {
            throw new BalanceIsNotEnoughException("Balance is not enough.");
        }
    }

    @Override
    public void createAnnouncement(String gameId, Announcement announcement) {
        marketRepository.getGameById(gameId);

        JSONObject announcementJson = new JSONObject();
        announcementJson.put("senderServiceName", "marketplace::" + new Date());
        announcementJson.put("gameId", gameId);
        announcementJson.put("title", announcement.getTitle());
        announcementJson.put("message", announcement.getMessage());
        sender.sendMessage(announcementJson.toString(), "announcement");
    }

    @Override
    public void setPriceByGameId(String gameId, JsonPatch patch) throws JsonPatchException, JsonProcessingException {
        Game game = marketRepository.getGameById(gameId);
        try {
            Game gamePatched = PatchHelper.applyPatch(patch, game);
            marketRepository.updateGame(gamePatched);
        } catch (Exception e) {

        }
    }

    @Override
    public void updateGame(String gameId, JsonPatch patch) throws JsonPatchException, JsonProcessingException {
        Game game = marketRepository.getGameById(gameId);
        try {
            Game gamePatched = PatchHelper.applyPatch(patch, game);
            String date = gamePatched.getReleaseDate();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date d = format.parse(date);
            System.out.println(d.toString());
            marketRepository.updateGame(gamePatched);
            checkPriceDecreasedAndSendMessage(game, gamePatched);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<TopGameResponse> getTrendingGames(int offset, int limit) {
        return marketRepository.getTrendingGames(offset, limit);
    }

    @Override
    public List<TopGameResponse> getTopSellerGames(int offset, int limit) {
        return marketRepository.getTopSellerGames(offset, limit);
    }

    @Override
    public List<TopGameResponse> getNewReleasesGames(int offset, int limit) {
        LocalDate now = LocalDate.now();
        LocalDate beforeAWeek = now.minusWeeks(1);
        return marketRepository.getNewReleasesGames(offset, limit, now.toString(), beforeAWeek.toString());
    }

    @Override
    public List<UpcomingGameResponse> getUpcomingGames(int offset, int limit) {
        LocalDate now = LocalDate.now();
        LocalDate afterTwoMonths = now.plusMonths(2);
        return marketRepository.getUpcomingGames(offset, limit, now.toString(), afterTwoMonths.toString());
    }
}
