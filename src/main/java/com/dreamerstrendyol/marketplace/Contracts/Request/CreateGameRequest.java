package com.dreamerstrendyol.marketplace.Contracts.Request;

import com.dreamerstrendyol.marketplace.Models.Language;
import com.dreamerstrendyol.marketplace.Models.Requirement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateGameRequest {
    @NotNull
    @Length(min = 1,max = 70)
    private String gameName;
    @NotNull
    private String gameDescription;
    @NotNull
    @Length(min = 1,max = 40)
    private String publisherCompany;
    @NotNull
    private String genre;
    @NotNull
    @Length(min = 1,max = 40)
    private String internetSite;
    @NotNull
    @Length(min = 1,max = 40)
    private String publisherSite;
    @NotNull
    @Min(value = 0,message = "Cant be minus value")
    private double price;
    @NotNull
    @Length(min = 1,max = 40)
    private String gameIcon;
    @NotNull
    private String hasAdultContent;
    @NotNull
    @Length(min = 1,max = 20)
    private String releaseDate;
    @NotNull
    private String[] tags;
    @NotNull
    private String[] platform;
    @NotNull
    private String[] gameMediaContainer;
    @NotNull
    private List<Requirement> systemRequirements;
    @NotNull
    private List<Language> languages;




}
