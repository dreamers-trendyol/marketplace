package com.dreamerstrendyol.marketplace.Contracts.Request;

import com.dreamerstrendyol.marketplace.Models.Language;
import com.dreamerstrendyol.marketplace.Models.Requirement;
import com.dreamerstrendyol.marketplace.Models.Review;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PatchGameRequest {
    @NotNull
    @Length(min = 1,max = 70)
    public String gameName;
    @NotNull
    public String gameDescription;
    @NotNull
    @Length(min = 1,max = 40)
    public String publisherCompany;
    @NotNull
    public String genre;
    @NotNull
    @Length(min = 1,max = 40)
    public String internetSite;
    @NotNull
    @Length(min = 1,max = 40)
    public String publisherSite;
    @NotNull
    @Min(value = 0,message = "Cant be minus value")
    public double price;
    @NotNull
    @Length(min = 1,max = 40)
    public String gameIcon;
    @NotNull
    public String hasAdultContent;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date releaseDate;
    @NotNull
    public String[] tags;
    @NotNull
    public String[] platform;
    @NotNull
    public String[] gameMediaContainer;
    @NotNull
    public List<Requirement> systemRequirements;
    @NotNull
    public List<Language> languages;
}
