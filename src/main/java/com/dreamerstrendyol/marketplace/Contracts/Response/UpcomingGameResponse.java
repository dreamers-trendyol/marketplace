package com.dreamerstrendyol.marketplace.Contracts.Response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpcomingGameResponse {
    private String gameId ;
    private String gameName;
    private String gameIcon;
    private String releaseDate;
    private String[] tags;

}
