package com.dreamerstrendyol.marketplace.Controllers;

import com.dreamerstrendyol.marketplace.Contracts.Request.CreateGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Request.PatchGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Request.ReviewValidationDTO;
import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Models.Announcement;
import com.dreamerstrendyol.marketplace.Models.Game;
import com.dreamerstrendyol.marketplace.Models.Review;
import com.dreamerstrendyol.marketplace.Services.MarketService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
class MarketControllerTest {

    private final String gameId = "cd035a98-76ba-4ebb-840c-4f641dacc87c";
    private final String gameName = "gameTest";
    private final String reviewId = "er035a98-76ba-4ebb-840c-4f6415acc87d";
    private final int page = 1;
    private final int offset = 0;
    private final int limit = 10;

    @Mock
    private MarketService marketService;
    private MarketController sut;
    private List<TopGameResponse> topGameResponses;

    @BeforeEach
    public void setUp() {
        sut = new MarketController();
        sut.setMarketService(marketService);

        topGameResponses = new ArrayList<>();
        TopGameResponse topGameResponse = new TopGameResponse();
        topGameResponse.setGameId(gameId);
        TopGameResponse topGameResponse2 = new TopGameResponse();
        topGameResponse2.setGameName(gameName);
        TopGameResponse topGameResponse3 = new TopGameResponse();
        topGameResponse3.setGameIcon(gameName);

        topGameResponses.add(topGameResponse);
        topGameResponses.add(topGameResponse2);
        topGameResponses.add(topGameResponse3);
    }

    @Test
    public void getGameById_whenCalled_shouldReturnOkAndGame() {
        //Arrange
        Game game = new Game();
        game.setGameId(gameId);
        doReturn(game).when(marketService).getGameById(gameId);

        //Act
        ResponseEntity<Game> result = sut.getGameById(gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).getGameId()).isEqualTo(gameId);
    }

    @Test
    public void getAllGame_whenCalled_shouldReturnOkAndAllGameList() {
        //Arrange
        Game game = new Game();
        game.setGameId(gameId);
        Game game2 = new Game();
        game2.setGameName(gameName);
        List<Game> gameList = new ArrayList<>();
        gameList.add(game);
        gameList.add(game2);
        doReturn(gameList).when(marketService).getAllGames();

        //Act
        ResponseEntity<List<Game>> result = sut.getAllGames();

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(2);
        assertThat(Objects.requireNonNull(result.getBody().get(0).getGameId())).isEqualTo(gameId);
        assertThat(Objects.requireNonNull(result.getBody().get(1).getGameName())).isEqualTo(gameName);
    }

    @Test
    public void createGame_whenCalled_shouldReturnCreatedAndLocation() {
        //Arrange
        URI location = URI.create(String.format("/v1/market/games/%s/", gameId));
        CreateGameRequest request = new CreateGameRequest();
        doReturn(gameId).when(marketService).createGame(request);

        //Act
        ResponseEntity<Void> result = sut.createGame(request);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(result.getHeaders().getLocation()).isEqualTo(location);
    }

    @Test
    public void deleteGame_whenCalled_shouldReturnNoContent() {
        //Arrange
        doNothing().when(marketService).deleteGame(gameId);

        //Act
        ResponseEntity<Void> result = sut.deleteGame(gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getPrice_whenCalled_shouldReturnOkAndPrice() {
        //Arrange
        Double price = 200D;
        doReturn(price).when(marketService).getPrice(gameId);

        //Act
        ResponseEntity<Double> result = sut.getPrice(gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(price);
    }

    @Test
    public void setPrice_whenCalled_shouldReturnNoContent() throws JsonPatchException, JsonProcessingException {
        //Arrange
        PatchGameRequest request = new PatchGameRequest();
        doNothing().when(marketService).setPriceByGameId(gameId, null);

        //Act
        ResponseEntity<Void> result = sut.setPrice(request, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getSalesCount_whenCalled_shouldReturnOkAndSalesCount() {
        //Arrange
        int salesCount = 10;
        doReturn(salesCount).when(marketService).getSalesCount(gameId);

        //Act
        ResponseEntity<Integer> result = sut.getSalesCount(gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(salesCount);
    }

    @Test
    public void addReview_whenCalled_shouldReturnCreatedAndLocation() {
        //Arrange
        URI location = URI.create(String.format("/v1/market/games/%s/reviews/%s/", gameId, reviewId));
        Review review = new Review();
        review.setReviewId(reviewId);
        doReturn(gameId).when(marketService).addReview(gameId, review);

        //Act
        ResponseEntity<Void> result = sut.addReview(review, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(result.getHeaders().getLocation()).isEqualTo(location);
    }

    @Test
    public void updateReview_whenCalled_shouldReturnNoContent() {
        //Arrange
        ReviewValidationDTO reviewValidationDTO = new ReviewValidationDTO();
        doNothing().when(marketService).updateReview(null, gameId, reviewId);

        //Act
        ResponseEntity<Void> result = sut.updateReview(reviewValidationDTO, gameId, reviewId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void deleteReview_whenCalled_shouldReturnNoContent() {
        //Arrange
        doNothing().when(marketService).deleteReview(gameId, reviewId);

        //Act
        ResponseEntity<Void> result = sut.deleteReview(reviewId, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getReviewById_whenCalled_shouldReturnOkAndReview() {
        //Arrange
        Review review = new Review();
        review.setReviewId(reviewId);
        doReturn(review).when(marketService).getReviewById(gameId, reviewId);

        //Act
        ResponseEntity<Review> result = sut.getReviewById(reviewId, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).getReviewId()).isEqualTo(reviewId);
    }

    @Test
    public void getUserRating_whenCalled_ReturnShouldOkAndUserRating() {
        //Arrange
        int userRating = 100;
        doReturn(userRating).when(marketService).getUserRating(gameId);

        //Act
        ResponseEntity<Integer> result = sut.getUserRating(gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(userRating);

    }

    @Test
    public void getReviewsByUserName_whenCalled_shouldReturnOkAndReviewList() {
        //Arrange
        String username = "username123";
        Review review = new Review();
        review.setReviewId(reviewId);
        Review review2 = new Review();
        review2.setUserName(username);
        List<Review> reviews = new ArrayList<>();
        reviews.add(review);
        reviews.add(review2);
        doReturn(reviews).when(marketService).getReviewsByUserName(gameId, username);

        //Act
        ResponseEntity<List<Review>> result = sut.getReviewsByUserName(username, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(2);
        assertThat(Objects.requireNonNull(result.getBody().get(0).getReviewId())).isEqualTo(reviewId);
        assertThat(Objects.requireNonNull(result.getBody().get(1).getUserName())).isEqualTo(username);
    }

    @Test
    public void getReviewsByUserName_whenCalledWithUsernameIsHyphen_shouldReturnOkAndReviewList() {
        //Arrange
        String username = "-";
        List<Review> reviewList = new ArrayList<>();
        Review review = new Review();
        review.setReviewId(reviewId);
        Review review2 = new Review();
        review2.setUserName(username);
        Review review3 = new Review();
        review3.setAdviceIt(true);
        reviewList.add(review);
        reviewList.add(review2);
        reviewList.add(review3);

        doReturn(reviewList).when(marketService).getReviews(gameId);

        //Act
        ResponseEntity<List<Review>> result = sut.getReviewsByUserName(username, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(3);
        assertThat(Objects.requireNonNull(result.getBody().get(0).getReviewId())).isEqualTo(reviewId);
        assertThat(Objects.requireNonNull(result.getBody().get(1).getUserName())).isEqualTo(username);
        assertThat(Objects.requireNonNull(result.getBody().get(2).getAdviceIt())).isEqualTo(true);
    }

    @Test
    public void buyGame_whenCalled_shouldReturnOk() {
        //Arrange
        String userId = "userId123";
        doNothing().when(marketService).buyGame(gameId, userId);

        //Act
        ResponseEntity<Void> result = sut.buyGame(gameId, userId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void createAnnouncement_whenCalled_shouldReturnOk() {
        //Arrange
        Announcement announcement = new Announcement("title", "message");
        doNothing().when(marketService).createAnnouncement(gameId, announcement);

        //Act
        ResponseEntity<Void> result = sut.createAnnouncement(gameId, announcement);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void updateGame_whenCalled_shouldReturnNoContent() throws JsonPatchException, JsonProcessingException {
        //Arrange
        PatchGameRequest request = new PatchGameRequest();
        doNothing().when(marketService).updateGame(gameId, null);

        //Act
        ResponseEntity<Void> result = sut.updateGame(request, gameId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getTrendingGames_whenCalled_shouldReturnOkAndTrendingGames() {
        //Arrange
        doReturn(topGameResponses).when(marketService).getTrendingGames(offset, limit);

        //Act
        ResponseEntity<List<TopGameResponse>> result = sut.getTrendingGames(page, limit);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(3);
        assertThat(Objects.requireNonNull(result.getBody()).get(0).getGameId()).isEqualTo(gameId);
        assertThat(Objects.requireNonNull(result.getBody()).get(1).getGameName()).isEqualTo(gameName);
        assertThat(Objects.requireNonNull(result.getBody()).get(2).getGameIcon()).isEqualTo(gameName);
    }

    @Test
    public void getTopSeller_whenCalled_shouldReturnOkAndTopSellerGames() {
        //Arrange
        doReturn(topGameResponses).when(marketService).getTopSellerGames(offset, limit);

        //Act
        ResponseEntity<List<TopGameResponse>> result = sut.getTopSellerGames(page, limit);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(3);
        assertThat(Objects.requireNonNull(result.getBody()).get(0).getGameId()).isEqualTo(gameId);
        assertThat(Objects.requireNonNull(result.getBody()).get(1).getGameName()).isEqualTo(gameName);
        assertThat(Objects.requireNonNull(result.getBody()).get(2).getGameIcon()).isEqualTo(gameName);
    }

    @Test
    public void getNewReleasesGames_whenCalled_shouldReturnOkAndNewReleasesGames() {
        //Arrange
        doReturn(topGameResponses).when(marketService).getNewReleasesGames(offset, limit);

        //Act
        ResponseEntity<List<TopGameResponse>> result = sut.getNewReleasesGames(page, limit);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(3);
        assertThat(Objects.requireNonNull(result.getBody()).get(0).getGameId()).isEqualTo(gameId);
        assertThat(Objects.requireNonNull(result.getBody()).get(1).getGameName()).isEqualTo(gameName);
        assertThat(Objects.requireNonNull(result.getBody()).get(2).getGameIcon()).isEqualTo(gameName);
    }

    @Test
    public void getUpcomingGames_whenCalled_shouldReturnOkAndUpcomingGames() {
        //Arrange
        String gameName = "gameUpcoming";
        UpcomingGameResponse upcomingGameResponse = new UpcomingGameResponse();
        upcomingGameResponse.setGameId(gameId);
        UpcomingGameResponse upcomingGameResponse2 = new UpcomingGameResponse();
        upcomingGameResponse2.setGameName(gameName);
        List<UpcomingGameResponse> upcomingGames = new ArrayList<>();
        upcomingGames.add(upcomingGameResponse);
        upcomingGames.add(upcomingGameResponse2);

        doReturn(upcomingGames).when(marketService).getUpcomingGames(offset, limit);

        //Act
        ResponseEntity<List<UpcomingGameResponse>> result = sut.getUpcomingGames(page, limit);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(result.getBody()).size()).isEqualTo(2);
        assertThat(Objects.requireNonNull(result.getBody()).get(0).getGameId()).isEqualTo(gameId);
        assertThat(Objects.requireNonNull(result.getBody()).get(1).getGameName()).isEqualTo(gameName);
    }
}